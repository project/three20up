<?php
// $Id: page.tpl.php,v 1.7 2010/09/17 21:36:06 eternalistic Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">

<!--
320 and Up boilerplate extension
Author: Andy Clarke
Version: 0.9b
URL: http://stuffandnonsense.co.uk/projects/320andup
-->

<!--[if IEMobile 7 ]><html class="no-js iem7"><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>"><!--<![endif]-->

<head>
<?php print $head ?>
<title><?php print $head_title; ?></title>
<meta name="description" content="">
<meta name="author" content="">

<!-- http://t.co/dKP3o1e -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1">

<!-- For less capable mobile browsers
<link rel="stylesheet" media="handheld" href="css/handheld.css?v=1">  -->

<!-- For all browsers -->
<?php print $styles; ?>

<!-- JavaScript at bottom except for Modernizr -->
<script src="/<?php echo path_to_theme(); ?>/js/libs/modernizr-1.7.min.js"></script>

<!-- For iPhone 4 -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png">
<!-- For Nokia -->
<link rel="shortcut icon" href="img/l/apple-touch-icon.png">
<!-- For everything else -->
<link rel="shortcut icon" href="/favicon.ico">

<!--iOS. Delete if not required -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="apple-touch-startup-image" href="img/splash.png">

<!--Microsoft. Delete if not required -->
<meta http-equiv="cleartype" content="on">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- Scripts -->
<?php print $scripts; ?>
</head>

<body id="<?php if (isset($body_id)) print $body_id; ?>" class="<?php print $body_classes; ?> clearfix">

<header role="banner" class="header-block clearfix">
<?php print $header_top; ?>
<?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
<?php endif; ?>
<?php if ($site_name): ?>
<h1 id="site-name">
  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
</h1>
<?php endif; ?>
<?php print $header; ?>
</header>

<div class="page content-page content clearfix">
<?php print $breadcrumb; ?>

<div role="main">
  <?php if ($title): ?>
    <h2 class="title"><?php print $title; ?></h2>
  <?php endif; ?>
  <?php if ($tabs): ?>
    <div class="tabs"><?php print $tabs; ?></div>
  <?php endif; ?>
  <?php print $messages; ?>
  <?php print $help; ?>
  <?php if ($content_top): ?>
    <?php print $content_top; ?>
  <?php endif; ?>
  <?php print $content; ?>
  <?php if ($content_bottom): ?>
    <?php print $content_bottom; ?>
  <?php endif; ?>
</div>

<div role="complementary">
  <?php print $sidebar_first; ?>
</div>

</div>

<footer role="contentinfo" class="footer clearfix">
  <div class="vcard"><?php print $footer_first; ?></div>
  <div><small><?php print $footer_last; ?></small></div>
</footer>

<!--[if (lt IE 9) & (!IEMobile)]>
<script src="/<?php echo path_to_theme(); ?>/js/libs/DOMAssistantCompressed-2.8.js"></script>
<script src="/<?php echo path_to_theme(); ?>/js/libs/selectivizr-1.0.1.js"></script>
<script src="/<?php echo path_to_theme(); ?>/js/libs/respond.min.js"></script>
<![endif]-->

<!-- http://t.co/HZe9oJ4 -->
  <?php if (isset($page_closure)) print $page_closure; ?>
  <?php print $closure; ?>
</body>
</html>
